package ru.kpfu.itis.Neil;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Nail Alishev
 */
public class Student {
    @JsonProperty("ID")
    private int id;
    @JsonProperty("Name")
    private String name;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
