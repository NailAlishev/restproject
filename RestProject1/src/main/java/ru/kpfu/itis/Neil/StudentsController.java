package ru.kpfu.itis.Neil;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.Neil.DAO.StudentsDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nail Alishev
 *         11-401
 */
@Controller
public class StudentsController {

    @RequestMapping(value = "/students", produces = "application/json")
    @ResponseBody
    public List<Student> getStudents() throws SQLException, ClassNotFoundException {
        return StudentsDAO.getAllStudents();
    }

    @RequestMapping(value = "/students/new", produces = "application/json")
    public String getStudentsCreatePage() {
        return "newStudent";
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public String createStudent(@RequestParam("id") int id, @RequestParam("name") String name) {
        //if (StudentsDAO.createStudent(id, name)) {
        //  return "redirect:/"
        //}
        return null;
    }

    @RequestMapping(value = "/students/{id}", produces = "application/json")
    @ResponseBody
    public Student getStudent(@PathVariable("id") int id) throws SQLException, ClassNotFoundException {
        return StudentsDAO.getStudentById(id);
    }

    @RequestMapping(value = "/students/{id}/edit", produces = "application/json")
    public String editStudentPage() {
        return "edit";
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.PATCH, produces = "application/json")
    @ResponseBody
    public Student editStudent() {
        return null;
    }

    @RequestMapping(value = "/students{id}", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public Student deleteStudent() {
        return null;
    }


}
