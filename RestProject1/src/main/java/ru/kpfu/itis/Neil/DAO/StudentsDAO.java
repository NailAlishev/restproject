package ru.kpfu.itis.Neil.DAO;

import ru.kpfu.itis.Neil.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nail Alishev
 *         11-401
 */
public class StudentsDAO {
    public static List<Student> getAllStudents() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        final String user = "neil";
        final String password = "";
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", user, password);
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery("select * from students");
        List<Student> students = new ArrayList<Student>();
        while (resultSet.next()) {
            students.add(new Student(resultSet.getInt("id"), resultSet.getString("name")));
        }
        return students;
    }

    public static Student getStudentById(int id) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        final String user = "neil";
        final String password = "";
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/virtual_schools", user, password);
        PreparedStatement stmt = conn.prepareStatement("select * from students where id = ?");
        stmt.setInt(1, id);
        ResultSet resultSet = stmt.executeQuery();
        resultSet.next();
        return new Student(resultSet.getInt("id"), resultSet.getString("name"));
    }
    public static void createStudent(){

    }
}
